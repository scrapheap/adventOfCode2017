package adventOfCode

import (
	"testing"
	"reflect"
)

func TestSlurp( t * testing.T ) {
	 type test struct {
        input       string
        want        []string
		shouldError bool
    }

	tests := []test{
        { input: "testdata/nonExistingFile", want: nil, shouldError: true },
        { input: "testdata/testFile", want: []string{ "ab", "cd", "ef" }, shouldError: false },
    }

	for _, testCase := range tests {
        got, err := Slurp( testCase.input )
		if testCase.shouldError {
			if err == nil {
				t.Fatal("Should have errored")
			}
		} else {
			if err != nil {
				t.Fatalf("errored with %v", err )
			} else {
				if !reflect.DeepEqual( testCase.want, got ) {
					t.Fatalf("expected: %v, got: %v", testCase.want, got)
				}
			}
		}
    }
}

func TestMin( t * testing.T ) {
	 type test struct {
        input       []int
        want        int
		shouldError bool
    }

	tests := []test{
        { input: []int{}, want: 0, shouldError: true },
        { input: []int{ 0, 1, 2, 3 }, want: 0, shouldError: false },
        { input: []int{ 1, 2, 3 }, want: 1, shouldError: false },
        { input: []int{ 3, 2 }, want: 2, shouldError: false },
        { input: []int{ -3, 2 }, want: -3, shouldError: false },
    }

	for _, testCase := range tests {
        got, err := Min( testCase.input... )
		if testCase.shouldError {
			if err == nil {
				t.Fatal("Should have errored")
			}
		} else {
			if err != nil {
				t.Fatalf("errored with %v", err )
			} else {
				if testCase.want != got {
					t.Fatalf("expected: %v, got: %v", testCase.want, got)
				}
			}
		}
    }
}

func TestMax( t * testing.T ) {
	 type test struct {
        input       []int
        want        int
		shouldError bool
    }

	tests := []test{
        { input: []int{}, want: 0, shouldError: true },
        { input: []int{ 0, 1, 2, 3 }, want: 3, shouldError: false },
        { input: []int{ 1, 2, 3 }, want: 3, shouldError: false },
        { input: []int{ 3, 2 }, want: 3, shouldError: false },
        { input: []int{ -3, 2 }, want: 2, shouldError: false },
    }

	for _, testCase := range tests {
        got, err := Max( testCase.input... )
		if testCase.shouldError {
			if err == nil {
				t.Fatal("Should have errored")
			}
		} else {
			if err != nil {
				t.Fatalf("errored with %v", err )
			} else {
				if testCase.want != got {
					t.Fatalf("expected: %v, got: %v", testCase.want, got)
				}
			}
		}
    }
}

func TestMode( t * testing.T ) {
	 type test struct {
        input       []int
        want        int
		shouldError bool
    }

	tests := []test{
        { input: []int{}, want: 0, shouldError: true },
        { input: []int{ 1 }, want: 1, shouldError: false },
        { input: []int{ 1, 2, 1 }, want: 1, shouldError: false },
        { input: []int{ 1, 1, 2 }, want: 1, shouldError: false },
        { input: []int{ -3, 2, -3 }, want: -3, shouldError: false },
    }

	for _, testCase := range tests {
        got, err := Mode( testCase.input... )
		if testCase.shouldError {
			if err == nil {
				t.Fatal("Should have errored")
			}
		} else {
			if err != nil {
				t.Fatalf("errored with %v", err )
			} else {
				if testCase.want != got {
					t.Fatalf("expected: %v, got: %v", testCase.want, got)
				}
			}
		}
    }
}

func TestMinRune( t * testing.T ) {
	 type test struct {
        input       []rune
        want        rune
		shouldError bool
    }

	tests := []test{
        { input: []rune{}, want: 0, shouldError: true },
        { input: []rune{ 0, 1, 2, 3 }, want: 0, shouldError: false },
        { input: []rune{ 1, 2, 3 }, want: 1, shouldError: false },
        { input: []rune{ 3, 2 }, want: 2, shouldError: false },
        { input: []rune{ -3, 2 }, want: -3, shouldError: false },
    }

	for _, testCase := range tests {
        got, err := MinRune( testCase.input... )
		if testCase.shouldError {
			if err == nil {
				t.Fatal("Should have errored")
			}
		} else {
			if err != nil {
				t.Fatalf("errored with %v", err )
			} else {
				if testCase.want != got {
					t.Fatalf("expected: %v, got: %v", testCase.want, got)
				}
			}
		}
    }
}

func TestMaxRune( t * testing.T ) {
	 type test struct {
        input       []rune
        want        rune
		shouldError bool
    }

	tests := []test{
        { input: []rune{}, want: 0, shouldError: true },
        { input: []rune{ 0, 1, 2, 3 }, want: 3, shouldError: false },
        { input: []rune{ 1, 2, 3 }, want: 3, shouldError: false },
        { input: []rune{ 3, 2 }, want: 3, shouldError: false },
        { input: []rune{ -3, 2 }, want: 2, shouldError: false },
    }

	for _, testCase := range tests {
        got, err := MaxRune( testCase.input... )
		if testCase.shouldError {
			if err == nil {
				t.Fatal("Should have errored")
			}
		} else {
			if err != nil {
				t.Fatalf("errored with %v", err )
			} else {
				if testCase.want != got {
					t.Fatalf("expected: %v, got: %v", testCase.want, got)
				}
			}
		}
    }
}

func TestCrt( t * testing.T ) {
	 type test struct {
        input       []int
		input2      []int
        want        int
		shouldError bool
    }

	tests := []test{
        { input: []int{ 13, 19, 3, 7, 5, 0},  input2: []int{ 11, 7, 1, 2, 2, 6 }, want: 376777, shouldError: true },
        { input: []int{ 13, 19, 3, 7, 5, 17}, input2: []int{ 11, 7, 1, 2, 2, 6 }, want: 376777, shouldError: false },
    }

	for _, testCase := range tests {
        got, err := Crt( testCase.input, testCase.input2 )
		if testCase.shouldError {
			if err == nil {
				t.Fatal("Should have errored")
			}
		} else {
			if err != nil {
				t.Fatalf("errored with %v", err )
			} else {
				if testCase.want != got {
					t.Fatalf("expected: %v, got: %v", testCase.want, got)
				}
			}
		}
    }
}

func TestSplit( t * testing.T ) {
	 type test struct {
        input       string
		input2		string
        want        []string
		shouldError bool
    }

	tests := []test{
        { input: `\s+`, input2: "a b c d", want: []string{"a", "b", "c", "d"} },
        { input: ``,    input2: "abcd",    want: []string{"a", "b", "c", "d"} },
    }

	for _, testCase := range tests {
        got := Split( testCase.input, testCase.input2 )
		if !reflect.DeepEqual( testCase.want, got ) {
			t.Fatalf("expected: %v, got: %v", testCase.want, got)
		}
    }
}

package main

import(
    "fmt"
	"os"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input []string ) string {
	progs := []rune(input[0])

	for _, move := range adventOfCode.Split( `\s*,\s*`, input[1] ) {
		chars := []rune( move )

		switch chars[0] {
			case 's':
				r, err := strconv.Atoi(string(chars[1:]))
				if err != nil {
					panic( err )
				}
				progs = append( progs[ len(progs) - r: ], progs[:len(progs) - r]... )
			case 'x':
				i:=1
				for i<len(chars) && chars[i] != '/' {
					i++
				}

				progA, err := strconv.Atoi(string(chars[1:i]))
				if err != nil {
					panic( err )
				}

				progB, err := strconv.Atoi(string(chars[i+1:]))
				if err != nil {
					panic( err )
				}

				progs[progA], progs[progB] = progs[progB], progs[progA]
			case 'p':
				i:=0
				for i<len(chars) && chars[i] != '/' {
					i++
				}
				progA := chars[i-1]
				progB := chars[i+1]

				a := -1
				b := -1

				for i, c := range progs {
					if c == progA {
						a = i
					}
					if c == progB {
						b = i
					}
				}
				progs[a], progs[b] = progs[b], progs[a]
		}
	}

	return string(progs)
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( append( []string{ "abcdefghijklmnop" }, input... ) )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := "abcdefghijklmnop"
	period := -1
	for i:=0;i<1000000000;i++ {
		answer2 = Part1( append( []string{answer2}, input... ) )
		if answer2 == "abcdefghijklmnop" {
			period = i+1
			break
		}
	}

	for i:=1000000000 - (1000000000 % period); i<1000000000; i++ {
		answer2 = Part1( append( []string{answer2}, input... ) )
	}

	fmt.Println( "answer 2: ", answer2 )
}

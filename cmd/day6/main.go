package main

import(
    "fmt"
	"os"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input string ) int {

	banks := []int{}

	for _, v := range adventOfCode.Split( `\s+`, input ) {
		val, err := strconv.Atoi( v )
		if err != nil {
			panic( err )
		}
		banks = append( banks, val )
	}

	n := len(banks)

	seen := map[string]bool{}

	for !seen[fmt.Sprintf( "%v", banks )] {
		seen[fmt.Sprintf( "%v", banks )] = true

		max := 0
		for i, v := range banks {
			if v > banks[max] {
				max = i
			}
		}

		i := max
		x := banks[max]
		banks[max] = 0
		for x > 0 {
			i = (i + 1) % n
			banks[i]++
			x--
		}
	}

	return len(seen)
}


func Part2( input string ) int {
	banks := []int{}

	for _, v := range adventOfCode.Split( `\s+`, input ) {
		val, err := strconv.Atoi( v )
		if err != nil {
			panic( err )
		}
		banks = append( banks, val )
	}

	n := len(banks)

	seen := map[string]int{}

	for seen[fmt.Sprintf( "%v", banks )] == 0 {
		seen[fmt.Sprintf( "%v", banks )] = len(seen)

		max := 0
		for i, v := range banks {
			if v > banks[max] {
				max = i
			}
		}

		i := max
		x := banks[max]
		banks[max] = 0
		for x > 0 {
			i = (i + 1) % n
			banks[i]++
			x--
		}
	}

	return len(seen) - seen[fmt.Sprintf( "%v", banks )]
}


func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input[0] )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input[0] )

	fmt.Println( "answer 2: ", answer2 )
}

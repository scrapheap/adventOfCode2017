package main

import(
    "fmt"
	"os"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input []string ) int {
	jumps := 0
	trampolines := []int{}

	for _, line := range input {
		v, err := strconv.Atoi( line )
		if err != nil {
			panic( err )
		}

		trampolines = append( trampolines, v )
	}

	p := 0;

	for p < len( trampolines ) {
		jumps++
		o := p
		p += trampolines[p]
		trampolines[o]++
	}

	return jumps
}

func Part2( input []string ) int {
	jumps := 0
	trampolines := []int{}

	for _, line := range input {
		v, err := strconv.Atoi( line )
		if err != nil {
			panic( err )
		}

		trampolines = append( trampolines, v )
	}

	p := 0;

	for p < len( trampolines ) {
		jumps++
		o := p
		p += trampolines[p]
		if trampolines[o] >= 3 {
			trampolines[o]--
		} else {
			trampolines[o]++
		}
	}

	return jumps
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}

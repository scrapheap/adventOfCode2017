package main

import(
    "fmt"
	"os"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input string ) int {
	str := []rune( input )

	level := 0
	score := 0
	inGarbage := false

	for i:=0; i<len(str); i++ {
		if inGarbage {
			switch str[i] {
				case '!':
					i++
				case '>':
					inGarbage = false
			}
		} else {
			switch str[i] {
				case '<':
					inGarbage = true
				case '{':
					level++
					score += level

				case '}':
					level--
			}
		}
	}

	return score
}

func Part2( input string ) int {
	str := []rune( input )

	score := 0
	inGarbage := false

	for i:=0; i<len(str); i++ {
		if inGarbage {
			switch str[i] {
				case '!':
					i++
				case '>':
					inGarbage = false
				default:
					score++
			}
		} else {
			switch str[i] {
				case '<':
					inGarbage = true
			}
		}
	}

	return score
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input[0] )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input[0] )

	fmt.Println( "answer 2: ", answer2 )
}

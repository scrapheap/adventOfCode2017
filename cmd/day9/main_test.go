package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input string
        want  int
    }

	tests := []test{
		{ input: "{}", want: 1 },
		{ input: "{{{}}}", want: 6 },
		{ input: "{{},{}}", want: 5 },
		{ input: "{{{},{},{{}}}}", want: 16 },
		{ input: "{<a>,<a>,<a>,<a>}", want: 1 },
		{ input: "{{<ab>},{<ab>},{<ab>},{<ab>}}", want: 9 },
		{ input: "{{<!!>},{<!!>},{<!!>},{<!!>}}", want: 9 },
		{ input: "{{<a!>},{<a!>},{<a!>},{<ab>}}", want: 3 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input string
        want  int
    }

	tests := []test{
		{ input: "<>", want: 0 },
		{ input: "<random characters>", want: 17 },
		{ input: "<<<<>", want: 3 },
		{ input: "<{!>}>", want: 2 },
		{ input: "<!!>", want: 0 },
		{ input: "<!!!>>", want: 0 },
		{ input: "<{o\"i!a,<{i<a>", want: 10 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	iterations = 2

	tests := []test{
		{ input: []string{
			"../.# => ##./#../...",
			".#./..#/### => #..#/..../..../#..#",
		}, want: 12 },
		{ input: []string{
			"../.# => ##./#../...",
			"#../#.#/##. => #..#/..../..../#..#",
		}, want: 12 },
		{ input: []string{
			"../.# => ##./#../...",
			"###/#../.#. => #..#/..../..../#..#",
		}, want: 12 },
		{ input: []string{
			"../.# => ##./#../...",
			".##/#.#/..# => #..#/..../..../#..#",
		}, want: 12 },
		{ input: []string{
			"../.# => ##./#../...",
			"###/..#/.#. => #..#/..../..../#..#",
		}, want: 12 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

package main

import(
    "fmt"
	"os"
	"regexp"
	"adventOfCode/internal/adventOfCode"
)

var iterations = 5

func Part1( input []string ) int {
	grid := [][]rune{{'.','#','.'},{'.','.','#'},{'#','#','#'}}

	rules := map[string][]rune{}

	parse := regexp.MustCompile( `([^ =]+)\s*=>\s*(.*)` )
	for _, line := range input {
		vals := parse.FindStringSubmatch( line )
		if len( vals ) > 0 {
			s := []rune{}
			for _, c := range vals[1] {
				if c != '/' {
					s = append(s, c )
				}
			}
			r := []rune{}
			for _, c := range vals[2] {
				if c != '/' {
					r = append( r, c )
				}
			}

			for _, p := range rotateAndFlip( s ) {
				rules[p] = r
			}
		}
	}

	for i := 0; i < iterations; i++ {
		newGrid := [][]rune{};

		if len(grid) % 2 == 0 {
			for newR, r:=0, 0; r < len(grid); newR, r = newR+3, r+2 {
				for t:=len(newGrid); t< newR+3; t++ {
					newGrid = append( newGrid, []rune{} );
				}
				for c:=0; c < len(grid[0]); c+=2 {
					block := string( []rune{ grid[r][c], grid[r][c+1], grid[r+1][c], grid[r+1][c+1] } )
					newBlock := rules[block]
					newGrid[newR] = append( newGrid[newR], newBlock[0], newBlock[1], newBlock[2] )
					newGrid[newR+1] = append( newGrid[newR+1], newBlock[3], newBlock[4], newBlock[5] )
					newGrid[newR+2] = append( newGrid[newR+2], newBlock[6], newBlock[7], newBlock[8] )
				}
			}
		} else {
			for newR, r :=0, 0; r < len(grid); newR, r = newR+4, r+3 {
				for t:=len(newGrid); t< newR+4; t++ {
					newGrid = append( newGrid, []rune{} );
				}

				for c:=0; c < len(grid[0]); c+=3 {
					block := string( []rune{ grid[r][c], grid[r][c+1], grid[r][c+2], grid[r+1][c], grid[r+1][c+1], grid[r+1][c+2], grid[r+2][c], grid[r+2][c+1], grid[r+2][c+2] } )
					newBlock := rules[block]
					newGrid[newR] = append( newGrid[newR], newBlock[0], newBlock[1], newBlock[2], newBlock[3] )
					newGrid[newR+1] = append( newGrid[newR+1], newBlock[4], newBlock[5], newBlock[6], newBlock[7] )
					newGrid[newR+2] = append( newGrid[newR+2], newBlock[8], newBlock[9], newBlock[10], newBlock[11] )
					newGrid[newR+3] = append( newGrid[newR+3], newBlock[12], newBlock[13], newBlock[14], newBlock[15] )
				}
			}
		}

		grid = newGrid
	}

	return countOn(grid)
}

func rotateAndFlip( s []rune ) []string {
	patterns := []string{ string( s ) }

	// rotate 90
	patterns = append( patterns, string( rotate( s ) ) )
	// rotate 180
	patterns = append( patterns, string( rotate( rotate( s ) ) ) )
	// rotate 270
	patterns = append( patterns, string( rotate( rotate( rotate( s ) ) ) ) )

	// flip horizontally
	patterns = append( patterns, string( hFlip( s ) ) )
	// rotate 90
	patterns = append( patterns, string( rotate( hFlip( s ) ) ) )
	// rotate 180
	patterns = append( patterns, string( rotate( rotate( hFlip( s ) ) ) ) )
	// rotate 270
	patterns = append( patterns, string( rotate( rotate( rotate( hFlip( s ) ) ) ) ) )

	// flip vertically
	patterns = append( patterns, string( vFlip( s ) ) )
	// rotate 90
	patterns = append( patterns, string( rotate( vFlip( s ) ) ) )
	// rotate 180
	patterns = append( patterns, string( rotate( rotate( vFlip( s ) ) ) ) )
	// rotate 270
	patterns = append( patterns, string( rotate( rotate( rotate( vFlip( s ) ) ) ) ) )

	return patterns
}

func rotate( block []rune ) []rune {
	if len(block) == 4 {
		return []rune{ block[2], block[0], block[3], block[1] }
	} else {
		return []rune{ block[6], block[3], block[0], block[7], block[4], block[1], block[8], block[5], block[2] }
	}
}

func hFlip( block []rune ) []rune {
	if len(block) == 4 {
		return []rune{ block[1], block[0], block[3], block[2] }
	} else {
		return []rune{ block[2], block[1], block[0], block[5], block[4], block[3], block[8], block[7], block[6] }
	}
}

func vFlip( block []rune ) []rune {
	if len(block) == 4 {
		return []rune{ block[2], block[3], block[0], block[1] }
	} else {
		return []rune{ block[6], block[7], block[8], block[3], block[4], block[5], block[0], block[1], block[2] }
	}
}

func countOn( grid [][]rune ) int {
	on := 0;

	for _, row := range grid {
		for _, cell := range row {
			if cell == '#' {
				on++
			}
		}
	}

	return on
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )


	iterations = 18

	answer2 := Part1( input )

	fmt.Println( "answer 2: ", answer2 )
}

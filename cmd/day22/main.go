package main

import(
    "fmt"
	"os"
	"adventOfCode/internal/adventOfCode"
)

type loc struct {
	x int
	y int
}

const(
	UP = 0
	RIGHT = 1
	DOWN = 2
	LEFT = 3
)

var moveForward = []loc{
	{ x: 0, y: -1 },
	{ x: 1, y: 0 },
	{ x: 0, y: 1 },
	{ x: -1, y: 0 },
}

func Part1( input []string ) int {
	grid := map[loc]rune{}

	maxY, maxX := 0, 0

	for y, line := range input {
		if( y > maxY ) {
			maxY = y
		}
		for x, cell := range line {
			if( x > maxX ) {
				maxX = x
			}
			grid[loc{x:x, y:y}] = cell
		}
	}

	pos := loc{x: (maxX/2), y: (maxY/2) }
	facing := UP

	infections:=0
	for i:=0; i<10000; i++ {
		if grid[pos] == '#' {
			facing = ( facing + 1 ) % 4
			grid[pos] = '.'
		} else {
			facing = ( facing + 3 ) % 4
			grid[pos] = '#'
			infections++
		}

		pos.x += moveForward[facing].x
		pos.y += moveForward[facing].y
	}

	return infections
}

func Part2( input []string ) int {
	grid := map[loc]rune{}

	maxY, maxX := 0, 0

	for y, line := range input {
		if( y > maxY ) {
			maxY = y
		}
		for x, cell := range line {
			if( x > maxX ) {
				maxX = x
			}
			grid[loc{x:x, y:y}] = cell
		}
	}

	pos := loc{x: (maxX/2), y: (maxY/2) }
	facing := UP

	infections:=0

	for i:=0; i<10000000; i++ {
		switch grid[pos] {
			case 0:
				grid[pos] = 'W'
				facing = ( facing + 3 ) % 4
			case '.':
				grid[pos] = 'W'
				facing = ( facing + 3 ) % 4
			case 'W':
				grid[pos] = '#'
				infections++
			case '#':
				grid[pos] = 'F'
				facing = ( facing + 1 ) % 4
			case 'F':
				grid[pos] = '.'
				facing = ( facing + 2 ) % 4
		}

		pos.x += moveForward[facing].x
		pos.y += moveForward[facing].y
	}

	return infections
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}

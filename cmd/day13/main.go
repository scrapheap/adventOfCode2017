package main

import(
    "fmt"
	"os"
	"strconv"
	"regexp"
	"container/list"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input []string ) int {
	layers := parse(input)

	severity := 0

	for l := layers.Front(); l != nil; l = l.Next() {
		layer := l.Value.(scanner);
		if (layer.id % period(layer.depth) ) == 0 {
			severity += layer.id * layer.depth
		}
	}
	return severity
}

func Part2( input []string ) int {
	layers := parse(input)


	wait := 0

	for wait >= 0 {
		caught := false
		for l := layers.Front(); l != nil; l = l.Next() {
			layer := l.Value.(scanner);
			if (( wait + layer.id) % period(layer.depth) ) == 0 {
				caught = true
				break
			}
		}

		if ! caught {
			return wait;
		}

		wait++
	}

	return 0
}

type scanner struct {
	id int
	depth int
}

func parse( input []string ) *list.List {
	layers := list.New()

	parseLine := regexp.MustCompile(`(\d+)`)

	for _, line := range input {
		details := parseLine.FindAllStringSubmatch( line, -1 )
		if len( details ) > 0 {
			l, err := strconv.Atoi( details[0][1] )
			if err != nil {
				panic( err )
			}

			p, err := strconv.Atoi( details[1][1] )
			if err != nil {
				panic( err )
			}

			layers.PushBack( scanner{id: l, depth: p } )
		}
	}

	return layers
}

func period(depth int) int {
	return (depth - 1) * 2
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}

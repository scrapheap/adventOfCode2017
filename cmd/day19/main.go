package main

import(
    "fmt"
	"os"
	"adventOfCode/internal/adventOfCode"
)

type loc struct {
	x int
	y int
}

func Part1( input []string ) string {
	grid := map[loc]rune{}

	for y, line := range input {
		for x, cell := range line {
			grid[loc{x:x, y:y}] = cell
		}
	}

	pos := loc{x:0, y:0}
	for grid[pos] != '|' {
		pos.x++
	}

	return string( walk(grid, pos, loc{x:pos.x, y:-1}, []rune{} ) )
}

func walk( grid map[loc]rune, pos loc, prev loc, path []rune ) []rune {
	newPos := pos
	switch grid[pos] {
		case ' ':
			return path
		case '+':
			if prev.x != pos.x {
				// switch to y
				if grid[loc{x:pos.x, y:pos.y-1}] != ' ' {
					newPos.y--
				} else {
					newPos.y++
				}
			} else {
				// switch to x
				if grid[loc{x:pos.x-1, y:pos.y}] != ' ' {
					newPos.x--
				} else {
					newPos.x++
				}
			}
		default:
			if grid[pos] != '|' && grid[pos] != '-' {
				path = append( path, grid[pos] )
			}

			if prev.x < pos.x {
				newPos.x++
			}

			if prev.x > pos.x {
				newPos.x--
			}

			if prev.y < pos.y {
				newPos.y++
			}

			if prev.y > pos.y {
				newPos.y--
			}
	}

	return walk( grid, newPos, pos, path )
}

func Part2( input []string ) int {
	grid := map[loc]rune{}

	for y, line := range input {
		for x, cell := range line {
			grid[loc{x:x, y:y}] = cell
		}
	}

	pos := loc{x:0, y:0}
	for grid[pos] != '|' {
		pos.x++
	}

	return countSteps(grid, pos, loc{x:pos.x, y:-1}, 0 )
}

func countSteps( grid map[loc]rune, pos loc, prev loc, steps int ) int {
	newPos := pos
	switch grid[pos] {
		case ' ':
			return steps
		case '+':
			if prev.x != pos.x {
				// switch to y
				if grid[loc{x:pos.x, y:pos.y-1}] != ' ' {
					newPos.y--
				} else {
					newPos.y++
				}
			} else {
				// switch to x
				if grid[loc{x:pos.x-1, y:pos.y}] != ' ' {
					newPos.x--
				} else {
					newPos.x++
				}
			}
		default:
			if prev.x < pos.x {
				newPos.x++
			}

			if prev.x > pos.x {
				newPos.x--
			}

			if prev.y < pos.y {
				newPos.y++
			}

			if prev.y > pos.y {
				newPos.y--
			}
	}

	return countSteps( grid, newPos, pos, steps + 1 )
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}

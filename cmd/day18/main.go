package main

import(
    "fmt"
	"os"
	"strconv"
	"time"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input []string ) int {
	regs := map[string]int{}
	playing := 0

	prog := [][]string{}

	for _, line := range input {
		prog = append( prog, adventOfCode.Split(`\s+`, line ) )
	}

	for i:=0; i>=0 && i<len(prog); i++ {
		instr := prog[i]
		switch instr[0] {
			case "snd": playing = val( regs, instr[1] )
			case "set": regs[instr[1]] = val( regs, instr[2] )
			case "add": regs[instr[1]] += val( regs, instr[2] )
			case "mul": regs[instr[1]] *= val( regs, instr[2] )
			case "mod": regs[instr[1]] %= val( regs, instr[2] )
			case "rcv": if val( regs, instr[1] ) != 0 {
				i = len(prog)
			}
			case "jgz": if val( regs, instr[1] ) > 0 {
				i += val( regs, instr[2] )
				i--
			}
		}
	}

	return playing
}

func val( regs map[string]int, operand string ) int {
	num, err := strconv.Atoi( operand )
	if err != nil {
		return regs[operand]
	}

	return num
}

func Part2( input []string ) int {
	prog := [][]string{}

	for _, line := range input {
		prog = append( prog, adventOfCode.Split(`\s+`, line ) )
	}

	ch1 := make( chan int, 100 )
	ch2 := make( chan int, 100 )

	go run( prog, 0, ch1, ch2 )

	return run( prog, 1, ch2, ch1 )
}

func run( prog [][]string, pid int, in, out chan int ) int {
	regs := map[string]int{ "p": pid }
	sent := 0;

	for i:=0; i>=0 && i<len(prog); i++ {
		instr := prog[i]
		switch instr[0] {
			case "snd":
				out <- val( regs, instr[1] )
				sent++
			case "set": regs[instr[1]] = val( regs, instr[2] )
			case "add": regs[instr[1]] += val( regs, instr[2] )
			case "mul": regs[instr[1]] *= val( regs, instr[2] )
			case "mod": regs[instr[1]] %= val( regs, instr[2] )
			case "rcv": 
				select {
					case m := <-in:
						regs[instr[1]] = m

					case <-time.After(3 * time.Second):
						i = len(prog)
				}
			case "jgz": if val( regs, instr[1] ) > 0 {
				i += val( regs, instr[2] )
				i--
			}
		}
	}

	return sent
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}

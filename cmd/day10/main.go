package main

import(
    "fmt"
	"os"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

var runningTests = false

func Part1( input string ) int {
	size := 256
	if runningTests {
		size = 5
	}

	knot := []int{}
	for i:=0; i<size; i++ {
		knot = append( knot, i )
	}

	lengths := []int{}
	for _, l := range adventOfCode.Split(`\s*,\s*`, input ) {
		length, err := strconv.Atoi( l )
		if err != nil {
			panic( err )
		}
		lengths = append(lengths, length)
	}

	skip := 0
	pos := 0

	for _, l := range lengths {
		tmp := []int{}
		for i:=0; i<l; i++ {
			tmp = append( tmp, knot[ ( pos + i ) % size ] )
		}

		for i:=0; i < len( tmp ); i++ {
			knot[ ( pos + i ) % size ] = tmp[ ( len(tmp) - 1 ) - i ]
		}

		pos += l + skip
		skip++
	}

	return knot[0] * knot[1]
}

func Part2( input string ) string {
	size := 256

	knot := []int{}
	for i:=0; i<size; i++ {
		knot = append( knot, i )
	}

	lengths := []int{}
	for _, l := range append([]rune(input), 17, 31, 73, 47, 23 ) {
		lengths = append( lengths, int(l) )
	}

	fmt.Println(lengths)
	skip := 0
	pos := 0

	for r:=0; r<64; r++ {
		for _, l := range lengths {
			tmp := []int{}
			for i:=0; i<l; i++ {
				tmp = append( tmp, knot[ ( pos + i ) % size ] )
			}

			for i:=0; i < len( tmp ); i++ {
				knot[ ( pos + i ) % size ] = tmp[ ( len(tmp) - 1 ) - i ]
			}

			pos += l + skip
			skip++
		}
	}

	hash := [16]byte{}
	for i:=0; i<256; i++ {
		hash[i/16] ^= byte(knot[i])
	}
	return fmt.Sprintf("%x", hash)
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input[0] )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input[0] )

	fmt.Println( "answer 2: ", answer2 )
}

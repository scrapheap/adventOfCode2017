package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input string
        want  int
    }

	tests := []test{
		{ input: "3, 4, 1, 5", want: 12 },
    }

	runningTests = true

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input string
        want  string
    }

	tests := []test{
		{ input: "", want: "a2582a3a0e66e6e86e3812dcb672a272" },
		{ input: "AoC 2017", want: "33efeb34ea91902bb2f59c9920caa6cd" },
		{ input: "1,2,3", want: "3efbe78a8d82f29979031a4aa0b16a9d" },
		{ input: "1,2,4", want: "63960835bcdc130f0b66d7ff4f6a5a8e" },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}


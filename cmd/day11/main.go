package main

import(
    "fmt"
	"os"
	"adventOfCode/internal/adventOfCode"
)

type loc struct {
	x int
	y int
}

func Part1( input string ) int {
	pos := loc{x:0, y:0}
	for _, d := range adventOfCode.Split(`\s*,\s*`, input ) {
		switch d {
			case `n`:
				pos.y++;

			case `ne`:
				pos.x++;

			case `se`:
				pos.y--;
				pos.x++;

			case `s`:
				pos.y--;

			case `sw`:
				pos.x--;

			case `nw`:
				pos.y++;
				pos.x--;
		}
	}

	return distanceFromOrigin( pos )
}

func abs(a int) int {
	if a < 0 {
		return -a
	}

	return a
}

func distanceFromOrigin( pos loc ) int {
	return ( abs( pos.x ) + abs( pos.y ) + abs( pos.x + pos.y ) ) / 2
}

func Part2( input string ) int {
	pos := loc{x:0, y:0}
	furthest := 0
	for _, d := range adventOfCode.Split(`\s*,\s*`, input ) {
		switch d {
			case `n`:
				pos.y++;

			case `ne`:
				pos.x++;

			case `se`:
				pos.y--;
				pos.x++;

			case `s`:
				pos.y--;

			case `sw`:
				pos.x--;

			case `nw`:
				pos.y++;
				pos.x--;
		}
		furthest, _ = adventOfCode.Max( furthest, distanceFromOrigin( pos ) )
	}

	return furthest
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input[0] )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input[0] )

	fmt.Println( "answer 2: ", answer2 )
}

package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input string
        want  int
    }

	tests := []test{
		{ input: "ne,ne,ne", want: 3 },
		{ input: "ne,ne,sw,sw", want: 0 },
		{ input: "ne,ne,s,s", want: 2 },
		{ input: "se,sw,se,sw,sw", want: 3 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input string
        want  int
    }

	tests := []test{
		{ input: "ne,ne,ne", want: 3 },
		{ input: "ne,ne,sw,sw", want: 2 },
		{ input: "ne,ne,s,s", want: 2 },
		{ input: "se,sw,se,sw,sw", want: 3 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

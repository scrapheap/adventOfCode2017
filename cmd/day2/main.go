package main

import(
    "fmt"
	"os"
	"regexp"
    "strconv"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input []string ) int {
	rows:= [][]int{}

	parse := regexp.MustCompile(`(\d+)`)

	for _, line := range input {
		row := []int{}

		for _, number := range parse.FindAllStringSubmatch( line, -1 ) {
			val, err := strconv.Atoi( number[1] );
			if err != nil {
				panic( err )
			}

			row = append( row, val )
		}

		rows = append( rows, row )
	}

	total := 0
	for _, row := range rows {
		max, err := adventOfCode.Max( row... )
		if err != nil {
			panic( err )
		}

		min, err := adventOfCode.Min( row... )
		if err != nil {
			panic( err )
		}
		total += max - min
	}

	return total
}

func Part2( input []string ) int {
	rows:= [][]int{}

	parse := regexp.MustCompile(`(\d+)`)

	for _, line := range input {
		row := []int{}

		for _, number := range parse.FindAllStringSubmatch( line, -1 ) {
			val, err := strconv.Atoi( number[1] );
			if err != nil {
				panic( err )
			}

			row = append( row, val )
		}

		rows = append( rows, row )
	}

	total := 0
	for _, row := range rows {
		for i:=0; i<len(row) - 1; i++ {
			for j:=i+1; j<len(row); j++ {
				max, err := adventOfCode.Max( row[i], row[j] )
				if err != nil {
					panic( err )
				}

				min, err := adventOfCode.Min( row[i], row[j] )
				if err != nil {
					panic( err )
				}

				if max % min == 0 {
					total += max / min
				}
			}
		}
	}

	return total
}


func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}

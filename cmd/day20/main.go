package main

import(
    "fmt"
	"os"
	"regexp"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

type xyz struct {
	x int
	y int
	z int
}

type particle struct {
	p xyz
	v xyz
	a xyz
}

func Part1( input []string ) int {
	particles := []particle{}

	parse := regexp.MustCompile( `p=<(-?\d+),(-?\d+),(-?\d+)>, v=<(-?\d+),(-?\d+),(-?\d+)>, a=<(-?\d+),(-?\d+),(-?\d+)>`)
	for _, line := range input {
		vals := parse.FindStringSubmatch( line )
		if len( vals ) > 0 {
			t := []int{}
			for _, v := range vals[1:] {
				d, _ := strconv.Atoi( v )
				t = append( t, d )
			}
			p := particle{
				p: xyz{x: t[0],y: t[1],z: t[2]},
				v: xyz{x: t[3],y: t[4],z: t[5]},
				a: xyz{x: t[6],y: t[7],z: t[8]},
			};
			particles = append( particles, p )
		}
	}


	for r:=0; r<1000; r++ {
		for i, _ := range particles {
			particles[i].v.x += particles[i].a.x;
			particles[i].v.y += particles[i].a.y;
			particles[i].v.z += particles[i].a.z;
			particles[i].p.x += particles[i].v.x;
			particles[i].p.y += particles[i].v.y;
			particles[i].p.z += particles[i].v.z;
		}
	}

	nearest := 0
	for i, p := range particles {
		if abs(p.p.x) + abs(p.p.y) + abs(p.p.z) < abs(particles[nearest].p.x) + abs(particles[nearest].p.y) + abs(particles[nearest].p.z) {
			nearest = i
		}
	}

	return nearest
}

func abs( v int ) int {
	if v < 0 {
		return -v
	}

	return v
}

func Part2( input []string ) int {
	particles := []particle{}

	parse := regexp.MustCompile( `p=<(-?\d+),(-?\d+),(-?\d+)>, v=<(-?\d+),(-?\d+),(-?\d+)>, a=<(-?\d+),(-?\d+),(-?\d+)>`)
	for _, line := range input {
		vals := parse.FindStringSubmatch( line )
		if len( vals ) > 0 {
			t := []int{}
			for _, v := range vals[1:] {
				d, _ := strconv.Atoi( v )
				t = append( t, d )
			}
			p := particle{
				p: xyz{x: t[0],y: t[1],z: t[2]},
				v: xyz{x: t[3],y: t[4],z: t[5]},
				a: xyz{x: t[6],y: t[7],z: t[8]},
			};
			particles = append( particles, p )
		}
	}


	for r:=0; r<1000; r++ {
		seen := map[xyz]int{}
		for i, _ := range particles {
			particles[i].v.x += particles[i].a.x;
			particles[i].v.y += particles[i].a.y;
			particles[i].v.z += particles[i].a.z;
			particles[i].p.x += particles[i].v.x;
			particles[i].p.y += particles[i].v.y;
			particles[i].p.z += particles[i].v.z;
			seen[particles[i].p]++
		}

		newParticles := []particle{}
		for _, p := range particles {
			if seen[p.p] == 1 {
				newParticles = append( newParticles, p )
			}
		}
		particles = newParticles
	}

	return len(particles)
}
func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}

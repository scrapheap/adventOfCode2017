package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
		{ input: []string{
			"p=<3,0,0>, v=<2,0,0>, a=<-1,0,0>",
			"p=<4,0,0>, v=<0,0,0>, a=<-2,0,0>",
		}, want: 0 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
		{ input: []string{
			"p=<-6,0,0>, v=<3,0,0>, a=<0,0,0>",
			"p=<-4,0,0>, v=<2,0,0>, a=<0,0,0>",
			"p=<-2,0,0>, v=<1,0,0>, a=<0,0,0>",
			"p=<3,0,0>, v=<-1,0,0>, a=<0,0,0>",
		}, want: 1 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

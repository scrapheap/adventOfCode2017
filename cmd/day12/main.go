package main

import(
    "fmt"
	"os"
	"strconv"
	"regexp"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input []string ) int {
	pipes := parse(input)

	seen := map[int]bool{}

	seen = walk(0, pipes, seen)

	return len(seen)
}

func Part2( input []string ) int {
	pipes := parse(input)

	seen := map[int]bool{}

	groupCount := 0;

	for p, _ := range pipes {
		if !seen[p] {
			groupCount++
			seen = walk( p, pipes, seen )
		}
	}

	return groupCount
}

func parse( input []string ) map[int][]int {
	pipes := map[int][]int{}

	parseLine := regexp.MustCompile(`(\d+)`)

	for _, line := range input {
		details := parseLine.FindAllStringSubmatch( line, -1 )
		if len( details ) > 0 {
			id, err := strconv.Atoi( details[0][1] )
			if err != nil {
				panic( err )
			}

			for _, c := range details[1:] {
				id2, err := strconv.Atoi( c[1] )
				if err != nil {
					panic( err )
				}
				pipes[id] = append( pipes[id], id2 )
			}
		}
	}

	return pipes
}

func walk( p int, pipes map[int][]int, seen map[int]bool ) map[int]bool {
	seen[p] = true;

	for _, c := range pipes[p] {
		if !seen[c] {
			seen = walk( c, pipes, seen )
		}
	}

	return seen
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}

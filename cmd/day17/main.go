package main

import(
    "fmt"
	"os"
	"strconv"
	"container/list"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input string ) int {
	step, err := strconv.Atoi( input )
	if err != nil {
		panic( err )
	}

	buffer := list.New()
	buffer.PushBack( 0 )
	cursor := buffer.Front()

	for i:=1; i<2018; i++ {
		for j:=0;j<step;j++ {
			cursor = cursor.Next()
			if cursor == nil {
				cursor = buffer.Front()
			}
		}
		cursor = buffer.InsertAfter( i, cursor )
	}

	return cursor.Next().Value.(int)
}

func Part2( input string ) int {
	step, err := strconv.Atoi( input )
	if err != nil {
		panic( err )
	}

	answer := 0
	pos := 0
	for i:=1; i<=50000000; i++ {
		pos = (pos + step ) % i
		if pos == 0 {
			answer = i
		}
		pos++
	}
	return answer
}

func dumpList( buffer *list.List ) []int {
	l := []int{}
	for elem := buffer.Front(); elem != nil; elem = elem.Next() {
		l = append( l, elem.Value.(int) )
	}

	return l
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input[0] )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input[0] )

	fmt.Println( "answer 2: ", answer2 )
}

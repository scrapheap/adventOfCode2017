package main

import(
    "fmt"
	"os"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

type connector struct {
	a int
	b int
}

func Part1( input []string ) int {
	connectors := []connector{}

	for _, line := range input {
		pins := adventOfCode.Split(`/`, line )
		pinA, err := strconv.Atoi(pins[0])
		if err != nil {
			panic( err )
		}
		pinB, err := strconv.Atoi(pins[1])
		if err != nil {
			panic( err )
		}
		connectors = append(connectors, connector{a: pinA, b: pinB } )
	}

	strongest := connect( connectors, 0, 0 )
	return strongest
}

func connect( connectors []connector, required int, strength int ) int {
	maxStrength := strength
	for i := 0; i < len( connectors ); i++ {
		if connectors[i].a == required {
			newConnectors := []connector{}
			newConnectors = append( newConnectors, connectors[:i]... )
			newConnectors = append( newConnectors, connectors[i+1:]... )
			maxStrength, _ = adventOfCode.Max(
				maxStrength,
				connect( newConnectors, connectors[i].b, strength + connectors[i].a + connectors[i].b ),
			)
		}
		if connectors[i].b == required {
			newConnectors := []connector{}
			newConnectors = append( newConnectors, connectors[:i]... )
			newConnectors = append( newConnectors, connectors[i+1:]... )
			maxStrength, _ = adventOfCode.Max(
				maxStrength,
				connect( newConnectors, connectors[i].a, strength + connectors[i].a + connectors[i].b ),
			)
		}
	}

	return maxStrength
}

func Part2( input []string ) int {
	connectors := []connector{}

	for _, line := range input {
		pins := adventOfCode.Split(`/`, line )
		pinA, err := strconv.Atoi(pins[0])
		if err != nil {
			panic( err )
		}
		pinB, err := strconv.Atoi(pins[1])
		if err != nil {
			panic( err )
		}
		connectors = append(connectors, connector{a: pinA, b: pinB } )
	}

	length := longest( connectors, 0, 0 )
	longestBridges := bridges( connectors, 0, length, []connector{} )
	maxStrength := 0
	for _, bridge := range longestBridges {
		strength := 0
		for _, con := range bridge {
			strength += con.a + con.b
		}
		maxStrength, _ = adventOfCode.Max( maxStrength, strength )
	}
	return maxStrength
}

func longest( connectors []connector, required int, length int ) int {
	maxLength:=length
	for i := 0; i < len( connectors ); i++ {
		if connectors[i].a == required {
			newConnectors := []connector{}
			newConnectors = append( newConnectors, connectors[:i]... )
			newConnectors = append( newConnectors, connectors[i+1:]... )
			maxLength, _ = adventOfCode.Max(
				maxLength,
				longest( newConnectors, connectors[i].b, length + 1 ),
			)
		}
		if connectors[i].b == required {
			newConnectors := []connector{}
			newConnectors = append( newConnectors, connectors[:i]... )
			newConnectors = append( newConnectors, connectors[i+1:]... )
			maxLength, _ = adventOfCode.Max(
				maxLength,
				longest( newConnectors, connectors[i].a, length + 1 ),
			)
		}
	}

	return maxLength
}

func bridges( connectors []connector, required int, length int, route []connector ) [][]connector {
	routes := [][]connector{};

	if length == 0 {
		return [][]connector{ route }
	}

	for i := 0; i < len( connectors ); i++ {
		if connectors[i].a == required {
			newConnectors := []connector{}
			newConnectors = append( newConnectors, connectors[:i]... )
			newConnectors = append( newConnectors, connectors[i+1:]... )
			newRoute := []connector{}
			newRoute = append( newRoute, route...)
			newRoute = append( newRoute, connectors[i] )
			routes = append( routes, bridges( newConnectors, connectors[i].b, length - 1, newRoute )... )
		}
		if connectors[i].b == required {
			newConnectors := []connector{}
			newConnectors = append( newConnectors, connectors[:i]... )
			newConnectors = append( newConnectors, connectors[i+1:]... )
			newRoute := []connector{}
			newRoute = append( newRoute, route...)
			newRoute = append( newRoute, connectors[i] )
			routes = append( routes, bridges( newConnectors, connectors[i].a, length - 1, newRoute )... )
		}
	}

	return routes
}


func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}

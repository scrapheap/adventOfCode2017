package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
		{ input: []string{
			"0/2",
			"2/2",
			"2/3",
			"3/4",
			"3/5",
			"0/1",
			"10/1",
			"9/10",
		}, want: 31 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
		{ input: []string{
			"0/2",
			"2/2",
			"2/3",
			"3/4",
			"3/5",
			"0/1",
			"10/1",
			"9/10",
		}, want: 19 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

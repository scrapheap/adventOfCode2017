package main

import(
    "fmt"
	"os"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input string ) int {
	digits := []rune(input)

	total := 0
	for i:=0; i<len( digits ); i++ {
		prev := i-1
		if prev<0 {
			prev = len(digits) -1
		}

		if digits[i] == digits[prev] {
			total += int( digits[i] - '0' )
		}
	}

	return total
}

func Part2( input string ) int {
	digits := []rune(input)

	total := 0
	halfway := len( digits ) / 2;
	for i:=0; i<halfway; i++ {
		if digits[i] == digits[ i + halfway ] {
			total += int( digits[i] - '0' )
		}
	}

	return total * 2
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input[0] )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input[0] )

	fmt.Println( "answer 2: ", answer2 )
}

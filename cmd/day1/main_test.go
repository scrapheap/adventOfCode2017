package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input string
        want  int
    }

	tests := []test{
		{ input: "1122", want: 3 },
		{ input: "1111", want: 4 },
		{ input: "1234", want: 0 },
		{ input: "91212129", want: 9 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}


func TestPart2( t * testing.T ) {
	 type test struct {
        input string
        want  int
    }

	tests := []test{
		{ input: "1212", want: 6 },
		{ input: "1221", want: 0 },
		{ input: "123425", want: 4 },
		{ input: "123123", want: 12 },
		{ input: "12131415", want: 4 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}


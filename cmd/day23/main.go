package main

import(
    "fmt"
	"os"
	"strconv"
	"adventOfCode/internal/adventOfCode"

	"github.com/fxtlabs/primes"
)

func Part1( input []string ) int {
	regs := map[string]int{}

	prog := [][]string{}

	for _, line := range input {
		prog = append( prog, adventOfCode.Split(`\s+`, line ) )
	}

	multiplies := 0
	for i:=0; i>=0 && i<len(prog); i++ {
		instr := prog[i]
		switch instr[0] {
			case "set": regs[instr[1]] = val( regs, instr[2] )
			case "sub": regs[instr[1]] -= val( regs, instr[2] )
			case "mul":
				regs[instr[1]] *= val( regs, instr[2] )
				multiplies++
			case "jnz": if val( regs, instr[1] ) != 0 {
				i += val( regs, instr[2] )
				i--
			}
		}
	}

	return multiplies
}

func Part2( input []string ) int {
	h := 0;

	for i:=109300; i<=126300; i+=17 {
		if ! primes.IsPrime(i) {
			h++;
		}
	}

	return h
}

func val( regs map[string]int, operand string ) int {
	num, err := strconv.Atoi( operand )
	if err != nil {
		return regs[operand]
	}

	return num
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}

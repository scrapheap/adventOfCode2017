package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input string
        want  int
    }

	tests := []test{
		{ input: "flqrgnkx", want: 8108 },
    }

	runningTests = true

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input string
        want  int
    }

	tests := []test{
		{ input: "flqrgnkx", want: 1242 },
    }

	runningTests = true

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

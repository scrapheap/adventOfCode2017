package main

import(
    "fmt"
	"os"
	"adventOfCode/internal/adventOfCode"
)

var runningTests = false

func Part1( input string ) int {
	block := [][]bool{}
	for i:=0; i < 128; i++ {
		block = append( block, hex2bool( knotHash( fmt.Sprintf( "%s-%d", input, i ) ) ) )
	}

	total := 0;

	for _, row := range block {
		for _, bit := range row {
			if bit {
				total++
			}
		}
	}

	return total
}

var bits = map[rune][]bool {
	'0': { false, false, false, false },
	'1': { false, false, false,  true },
	'2': { false, false,  true, false },
	'3': { false, false,  true,  true },
	'4': { false,  true, false, false },
	'5': { false,  true, false,  true },
	'6': { false,  true,  true, false },
	'7': { false,  true,  true,  true },
	'8': {  true, false, false, false },
	'9': {  true, false, false,  true },
	'a': {  true, false,  true, false },
	'b': {  true, false,  true,  true },
	'c': {  true,  true, false, false },
	'd': {  true,  true, false,  true },
	'e': {  true,  true,  true, false },
	'f': {  true,  true,  true,  true },
}

func hex2bool(hex string) []bool {
	output :=[]bool{}

	for _, c := range hex {
		output = append( output, bits[c]... )
	}

	return output
}

func knotHash(input string) string {
	size := 256

	knot := []int{}
	for i:=0; i<size; i++ {
		knot = append( knot, i )
	}

	lengths := []int{}
	for _, l := range append([]rune(input), 17, 31, 73, 47, 23 ) {
		lengths = append( lengths, int(l) )
	}

	skip := 0
	pos := 0

	for r:=0; r<64; r++ {
		for _, l := range lengths {
			tmp := []int{}
			for i:=0; i<l; i++ {
				tmp = append( tmp, knot[ ( pos + i ) % size ] )
			}

			for i:=0; i < len( tmp ); i++ {
				knot[ ( pos + i ) % size ] = tmp[ ( len(tmp) - 1 ) - i ]
			}

			pos += l + skip
			skip++
		}
	}

	hash := [16]byte{}
	for i:=0; i<256; i++ {
		hash[i/16] ^= byte(knot[i])
	}
	return fmt.Sprintf("%x", hash)
}

type loc struct {
	x int
	y int
}

func Part2( input string ) int {
	block := [][]bool{}
	for i:=0; i < 128; i++ {
		block = append( block, hex2bool( knotHash( fmt.Sprintf( "%s-%d", input, i ) ) ) )
	}


	groups := map[loc]int{}
	group := 0

	pos := loc{}
	for pos.y=0; pos.y<128; pos.y++ {
		for pos.x=0; pos.x<128; pos.x++ {
			if block[pos.y][pos.x] && groups[pos] == 0 {
				group++
				groups = flood(pos, block, groups, group)
			}
		}
	}

	return group
}

func flood(pos loc, block [][]bool, groups map[loc]int, group int) map[loc]int {
	groups[pos] = group

	for _, neighbour := range neighbours( pos ) {
		if block[neighbour.y][neighbour.x] && groups[neighbour] == 0 {
			groups = flood( neighbour, block, groups, group )
		}
	}

	return groups
}

func neighbours( pos loc ) []loc {
	n := []loc{}
	if pos.x > 0 {
		n = append( n, loc{ x: pos.x - 1, y: pos.y } )
	}
	if pos.y > 0 {
		n = append( n, loc{ x: pos.x, y: pos.y - 1 } )
	}
	if pos.x < 127 {
		n = append( n, loc{ x: pos.x + 1, y: pos.y } )
	}
	if pos.y < 127 {
		n = append( n, loc{ x: pos.x, y: pos.y + 1 } )
	}

	return n
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input[0] )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input[0] )

	fmt.Println( "answer 2: ", answer2 )
}

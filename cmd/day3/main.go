package main

import(
    "fmt"
	"os"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

type loc struct {
	x int
	y int
}

const (
	NORTH = 3
	EAST = 2
	SOUTH = 1
	WEST = 0
)

func Part1( input string ) int {
	target, err := strconv.Atoi( input )
	if err != nil {
		panic( err )
	}

	move := map[int]loc{
		NORTH: { x:0,  y:-1 },
		EAST:  { x:1,  y:0 },
		SOUTH: { x:0,  y:1 },
		WEST:  { x:-1, y: 0 },
	}

	mem := map[loc]int{};

	pos := loc{ x: 0, y: 0 }
	facing := EAST

	currentNumber := 1
	for currentNumber < target {
		mem[pos] = currentNumber
		pos = add( pos, move[facing] )
		// check if left is clear and if it is turn to face left
		left := (facing +1 ) % 4
		if mem[add( pos, move[left] ) ] == 0 {
			facing = left
		}
		currentNumber++
	}

	if pos.x < 0 {
		pos.x -= (pos.x + pos.x)
	}

	if pos.y < 0 {
		pos.y -= (pos.y + pos.y)
	}

	return pos.x + pos.y
}

func add( a loc, b loc ) loc {
	return loc{
		x: a.x + b.x,
		y: a.y + b.y,
	}
}

func Part2( input string ) int {
	target, err := strconv.Atoi( input )
	if err != nil {
		panic( err )
	}

	move := map[int]loc{
		NORTH: { x:0,  y:-1 },
		EAST:  { x:1,  y:0 },
		SOUTH: { x:0,  y:1 },
		WEST:  { x:-1, y: 0 },
	}

	neighbours := []loc{
		{ x:0,  y:-1 },
		{ x:1,  y:-1 },
		{ x:1,  y:0 },
		{ x:1,  y:1 },
		{ x:0,  y:1 },
		{ x:-1, y:1 },
		{ x:-1, y: 0 },
		{ x:-1, y: -1 },
	}

	mem := map[loc]int{};

	pos := loc{ x: 0, y: 0 }
	facing := EAST

	currentNumber := 0
	for currentNumber <= target {
		currentNumber = 0
		for _, n := range neighbours {
			currentNumber += mem[add( pos, n )]
		}

		if currentNumber == 0 {
			currentNumber = 1
		}

		mem[pos] = currentNumber
		pos = add( pos, move[facing] )
		// check if left is clear and if it is turn to face left
		left := (facing +1 ) % 4
		if mem[add( pos, move[left] ) ] == 0 {
			facing = left
		}
	}

	return currentNumber
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input[0] )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input[0] )

	fmt.Println( "answer 2: ", answer2 )
}

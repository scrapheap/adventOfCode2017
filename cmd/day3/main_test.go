package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input string
        want  int
    }

	tests := []test{
		{ input: "1", want: 0 },
		{ input: "12", want: 3 },
		{ input: "23", want: 2 },
		{ input: "1024", want: 31 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input string
        want  int
    }

	tests := []test{
		{ input: "1", want: 2 },
		{ input: "2", want: 4 },
		{ input: "3", want: 4 },
		{ input: "4", want: 5 },
		{ input: "5", want: 10 },
		{ input: "6", want: 10 },
		{ input: "805", want: 806 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

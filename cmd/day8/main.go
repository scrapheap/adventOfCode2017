package main

import(
    "fmt"
	"os"
	"strconv"
	"regexp"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input []string ) int {
	cond := map[string]func( int, int ) bool {
		"<": func (a, b int) bool { return a < b },
		">": func (a, b int) bool { return a > b },
		"<=": func (a, b int) bool { return a <= b },
		">=": func (a, b int) bool { return a >= b },
		"!=": func (a, b int) bool { return a != b },
		"==": func (a, b int) bool { return a == b },
	}

	ops := map[string]func( int, int ) int {
		"inc": func( a, b int) int { return a + b },
		"dec": func( a, b int) int { return a - b },
	}

	parseNode := regexp.MustCompile(`(\w+)\s+(\w+)\s+(-?\w+)\s+if\s+(\w+)\s*(<|>|<=|>=|!=|==)\s*(-?\w+)`)

	regs := map[string]int{}

	for _, line := range input {
		parts := parseNode.FindStringSubmatch( line )
		if len( parts ) > 0 {
			val, err := strconv.Atoi( parts[6] )
			if err != nil {
				panic( err )
			}

			if cond[parts[5]]( regs[parts[4]], val ) {
				val, err := strconv.Atoi( parts[3] )
				if err != nil {
					panic( err )
				}
				regs[parts[1]] = ops[parts[2]]( regs[parts[1]], val )
			}
		}
	}

	vals := []int{}
	for _, v := range regs {
		vals = append( vals, v )
	}

	max, err := adventOfCode.Max( vals... )
	if err != nil {
		panic( err )
	}

	return max
}


func Part2( input []string ) int {
	cond := map[string]func( int, int ) bool {
		"<": func (a, b int) bool { return a < b },
		">": func (a, b int) bool { return a > b },
		"<=": func (a, b int) bool { return a <= b },
		">=": func (a, b int) bool { return a >= b },
		"!=": func (a, b int) bool { return a != b },
		"==": func (a, b int) bool { return a == b },
	}

	ops := map[string]func( int, int ) int {
		"inc": func( a, b int) int { return a + b },
		"dec": func( a, b int) int { return a - b },
	}

	parseNode := regexp.MustCompile(`(\w+)\s+(\w+)\s+(-?\w+)\s+if\s+(\w+)\s*(<|>|<=|>=|!=|==)\s*(-?\w+)`)

	regs := map[string]int{}

	max := 0
	for _, line := range input {
		parts := parseNode.FindStringSubmatch( line )
		if len( parts ) > 0 {
			val, err := strconv.Atoi( parts[6] )
			if err != nil {
				panic( err )
			}

			if cond[parts[5]]( regs[parts[4]], val ) {
				val, err := strconv.Atoi( parts[3] )
				if err != nil {
					panic( err )
				}
				regs[parts[1]] = ops[parts[2]]( regs[parts[1]], val )
				if regs[parts[1]] > max {
					max = regs[parts[1]]
				}
			}
		}
	}

	return max
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}

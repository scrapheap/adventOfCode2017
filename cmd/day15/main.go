package main

import(
    "fmt"
	"os"
	"strconv"
	"regexp"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input []string ) int {
	seedA, seedB := parse(input)

	matches := 0
	for i:=0; i<40000000; i++ {
		seedA = next( seedA, 16807 )
		seedB = next( seedB, 48271 )

		if ( seedA & 0xFFFF ) == ( seedB & 0xFFFF ) {
			matches++
		}
	}

	return matches
}

func next( seed int, factor int ) int {
	return ( seed * factor ) % 2147483647
}

func parse( input []string ) (int, int) {
	parseLine := regexp.MustCompile(`(\d+)`)

	seedA := 0
	seedB := 0

	details := parseLine.FindAllStringSubmatch( input[0], -1 )
	if len( details ) > 0 {
		seedA, _ = strconv.Atoi( details[0][1] )
	}
	details = parseLine.FindAllStringSubmatch( input[1], -1 )
	if len( details ) > 0 {
		seedB, _ = strconv.Atoi( details[0][1] )
	}

	return seedA, seedB
}


func Part2( input []string ) int {
	seedA, seedB := parse(input)

	matches := 0
	for i:=0; i<5000000; i++ {
		seedA = ( seedA * 16807 ) % 2147483647
		for seedA & 3 != 0 {
			seedA = ( seedA * 16807 ) % 2147483647
		}

		seedB = ( seedB * 48271 ) % 2147483647
		for seedB & 7 != 0 {
			seedB = ( seedB * 48271 ) % 2147483647
		}

		if ( seedA & 0xFFFF ) == ( seedB & 0xFFFF ) {
			matches++
		}
	}

	return matches
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}

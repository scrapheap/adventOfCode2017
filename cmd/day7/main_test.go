package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  string
    }

	tests := []test{
		{ input: []string{
			"pbga (66)",
			"xhth (57)",
			"ebii (61)",
			"havc (66)",
			"ktlj (57)",
			"fwft (72) -> ktlj, cntj, xhth",
			"qoyq (66)",
			"padx (45) -> pbga, havc, qoyq",
			"tknk (41) -> ugml, padx, fwft",
			"jptl (61)",
			"ugml (68) -> gyxo, ebii, jptl",
			"gyxo (61)",
			"cntj (57)",
		}, want: "tknk" },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
		{ input: []string{
			"pbga (66)",
			"xhth (57)",
			"ebii (61)",
			"havc (66)",
			"ktlj (57)",
			"fwft (72) -> ktlj, cntj, xhth",
			"qoyq (66)",
			"padx (45) -> pbga, havc, qoyq",
			"tknk (41) -> ugml, padx, fwft",
			"jptl (61)",
			"ugml (68) -> gyxo, ebii, jptl",
			"gyxo (61)",
			"cntj (57)",
		}, want: 60 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

package main

import(
    "fmt"
	"os"
	"strconv"
	"regexp"
	"adventOfCode/internal/adventOfCode"
)

type node struct {
	weight int
	children []string
	totalWeight int
}

func Part1( input []string ) string {
	progs := parse(input)

	possibleRoots := map[string]bool{};

	for name, _ := range progs {
		possibleRoots[name] = true
	}

	for _, node := range progs {
		for _, child := range node.children {
			delete( possibleRoots, child )
		}
	}

	for name, _ := range possibleRoots {
		return name
	}
	return ""
}

func parse( input []string ) map[string]node {
	nodes := map[string]node{}

	parseNode := regexp.MustCompile(`(\w+)`)

	for _, line := range input {
		details := parseNode.FindAllStringSubmatch( line, -1 )
		if len( details ) > 0 {
			weight, err := strconv.Atoi( details[1][1] )
			if err != nil {
				panic( err )
			}

			children := []string{}
			for _, c := range details[2:] {
				children = append(children, c[1])
			}

			prog := node{
				weight: weight,
				children: children,
			}

			nodes[details[0][1]] = prog
		}
	}

	return nodes
}

func Part2( input []string ) int {
	progs := parse(input)

	possibleRoots := map[string]bool{};

	for name, _ := range progs {
		possibleRoots[name] = true
	}

	for _, node := range progs {
		for _, child := range node.children {
			delete( possibleRoots, child )
		}
	}

	root := ""
	for name, _ := range possibleRoots {
		root = name
	}

	calculateTotalWeights( progs, root )
	return inbalance( progs, root, 0 )

	return 0
}

func calculateTotalWeights( progs map[string]node, current string ) int {
	node := progs[current]
	node.totalWeight = node.weight

	for _, child := range node.children {
		node.totalWeight += calculateTotalWeights( progs, child )
	}

	progs[current] = node
	return node.totalWeight
}

func inbalance( progs map[string]node, current string, expected int ) int {
	childWeights := []int{};
	for _, child := range progs[current].children {
		childWeights = append( childWeights, progs[child].totalWeight )
	}

	expect, err := adventOfCode.Mode( childWeights... );
	if err != nil {
		panic( err )
	}

	unbalancedChild := ""

	for _, child := range progs[current].children {
		expected -= progs[child].totalWeight
		if progs[child].totalWeight != expect {
			unbalancedChild = child
		}
	}

	if unbalancedChild != "" {
		return inbalance( progs, unbalancedChild, expect )
	}

	return expected
}


func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}

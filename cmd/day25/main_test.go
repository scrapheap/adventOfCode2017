package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
		{ input: []string{
			"Begin in state A.",
			"Perform a diagnostic checksum after 6 steps.",
			"",
			"In state A:",
			"  If the current value is 0:",
			"    - Write the value 1.",
			"    - Move one slot to the right.",
			"    - Continue with state B.",
			"  If the current value is 1:",
			"    - Write the value 0.",
			"    - Move one slot to the left.",
			"    - Continue with state B.",
			"",
			"In state B:",
			"  If the current value is 0:",
			"    - Write the value 1.",
			"    - Move one slot to the left.",
			"    - Continue with state A.",
			"  If the current value is 1:",
			"    - Write the value 1.",
			"    - Move one slot to the right.",
			"    - Continue with state A.",
		}, want: 3 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

package main

import(
    "fmt"
	"os"
	"regexp"
	"strconv"
	"adventOfCode/internal/adventOfCode"
)

type state struct {
	write [2]int
	move  [2]int
	next  [2]string
}

func Part1( input []string ) int {
	tape := map[int]int{}

	currentState := ""
	steps := 0


	initialState := regexp.MustCompile(`Begin in state (\w+)\.`)
	checksumAfter := regexp.MustCompile(`Perform a diagnostic checksum after (\d+) steps.`)
	changeState := regexp.MustCompile(`In state (\w+):`)
	changeValue := regexp.MustCompile(`If the current value is (\d+):`)
	writeValue := regexp.MustCompile(`Write the value (\d+).`)
	moveValue := regexp.MustCompile(`Move one slot to the (\w+).`)
	continueValue := regexp.MustCompile(`Continue with state (\w+).`)

	states := map[string]state{};
	parsingState := ""

	workingState := new(state)
	workingValue := 0

	for _, line := range input {
		parts := initialState.FindStringSubmatch( line )
		if len( parts ) > 0 {
			currentState = parts[1]
		}

		parts = checksumAfter.FindStringSubmatch( line )
		if len( parts ) > 0 {
			steps, _ = strconv.Atoi(parts[1])
		}

		parts = changeState.FindStringSubmatch( line )
		if len( parts ) > 0 {
			if parsingState != "" {
				states[parsingState] = *workingState
				workingState = new(state)
			}
			parsingState = parts[1]
		}

		parts = changeValue.FindStringSubmatch( line )
		if len( parts ) > 0 {
			workingValue, _ = strconv.Atoi(parts[1])
		}

		parts = writeValue.FindStringSubmatch( line )
		if len( parts ) > 0 {
			workingState.write[workingValue], _ = strconv.Atoi(parts[1])
		}

		parts = moveValue.FindStringSubmatch( line )
		if len( parts ) > 0 {
			if parts[1] == "left" {
				workingState.move[workingValue] = -1
			} else {
				workingState.move[workingValue] = 1
			}
		}

		parts = continueValue.FindStringSubmatch( line )
		if len( parts ) > 0 {
			workingState.next[workingValue] = parts[1]
		}
	}

	states[parsingState] = *workingState

	cursor := 0
	for steps > 0 {
		v := tape[cursor]
		tape[cursor] = states[currentState].write[v]
		cursor += states[currentState].move[v]
		currentState = states[currentState].next[v]
		steps--
	}

	checksum := 0
	for _, b := range tape {
		checksum += b
	}

	return checksum
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )
}

package main

import(
    "fmt"
	"os"
	"sort"
	"adventOfCode/internal/adventOfCode"
)

func Part1( input []string ) int {
	valid := 0
	for _, line := range input {
		if validPassword( line ) {
			valid++
		}
	}

	return valid
}

func validPassword( password string ) bool {
	seen := map[string]bool{}

	for _, word := range adventOfCode.Split(`\s+`, password) {
		if seen[word] {
			return false
		}
		seen[word] = true
	}

	return true
}

func Part2( input []string ) int {
	valid := 0
	for _, line := range input {
		if validPassword2( line ) {
			valid++
		}
	}

	return valid
}

func validPassword2( password string ) bool {
	seen := map[string]bool{}

	for _, word := range adventOfCode.Split(`\s+`, password) {
		word = sortWord( word )
		if seen[word] {
			return false
		}
		seen[word] = true
	}

	return true
}

func sortWord( word string ) string {
	letters := []rune( word )

	sort.Slice(letters, func (i,j int) bool {
		return letters[i] < letters[j]
	} )

	return string(letters)
}

func main() {
	if len( os.Args ) < 2 {
		fmt.Println("usage ", os.Args[0], " <DAY_DATA_FILE>")
		os.Exit(1);
	}

	input, err := adventOfCode.Slurp(os.Args[1])

	if err != nil {
		panic( err )
	}

	answer1 := Part1( input )

	fmt.Println( "answer 1: ", answer1 )

	answer2 := Part2( input )

	fmt.Println( "answer 2: ", answer2 )
}

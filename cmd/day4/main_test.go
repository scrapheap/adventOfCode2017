package main

import (
	"testing"
)

func TestPart1( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
		{ input: []string{ "aa bb cc dd ee" }, want: 1 },
		{ input: []string{ "aa bb cc dd aa" }, want: 0 },
		{ input: []string{ "aa bb cc dd aaa" }, want: 1 },
		{ input: []string{
			"aa bb cc dd ee",
			"aa bb cc dd aa",
			"aa bb cc dd aaa",
		}, want: 2 },
    }

	for _, testCase := range tests {
        got := Part1( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}

func TestPart2( t * testing.T ) {
	 type test struct {
        input []string
        want  int
    }

	tests := []test{
		{ input: []string{ "abcde fghij" }, want: 1 },
		{ input: []string{ "abcde xyz ecdab" }, want: 0 },
		{ input: []string{ "a ab abc abd abf abj" }, want: 1 },
		{ input: []string{ "iiii oiii ooii oooi oooo" }, want: 1 },
		{ input: []string{ "oiii ioii iioi iiio" }, want: 0 },
    }

	for _, testCase := range tests {
        got := Part2( testCase.input )
        if testCase.want != got {
            t.Fatalf("expected: %v, got: %v", testCase.want, got)
        }
    }
}
